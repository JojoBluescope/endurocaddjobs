#-------------------------------------------------
#    Program WallBuilder 1.9.0.0 1.9.0.0
#    wallpanel_2 (Panel 2)
#-------------------------------------------------

   ~adata.wallpanel_2.builtby = 'WallBuilder 1.9.0.0'


# See line 0 of 
	this = null
   get $HFCMODELS/C9075RA.e, -p=(1.0000,2.0000,90.0000), -g=grp_wall, -col=g_colour_stud75, -fil=grey50,\
            -z=2436, -ya=(0.0000,0.0000,-1.0000), -za=(0.0000,1.0000,0.0000)
   this.att[1]   = "STUD"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "C9075ra|0.75|SQUARE"
   this.att[5]   = "1"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 1
   this.fragment = 1



# See line 0 of 
	this = null
   get $HFCMODELS/C9075RA.e, -p=(1909.0000,2438.0000,90.0000), -g=grp_wall, -col=g_colour_stud75, -fil=grey50,\
            -z=2436, -ya=(0.0000,0.0000,-1.0000), -za=(0.0000,-1.0000,0.0000)
   this.att[1]   = "STUD"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "C9075ra|0.75|SQUARE"
   this.att[5]   = "2"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 1
   this.fragment = 1



# See line 0 of 
	this = null
   get $HFCMODELS/C9075RA.e, -p=(601.0000,2.0000,90.0000), -g=grp_wall, -col=g_colour_stud75, -fil=grey50,\
            -z=2436, -ya=(0.0000,0.0000,-1.0000), -za=(0.0000,1.0000,0.0000)
   this.att[1]   = "STUD"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "C9075ra|0.75|SQUARE"
   this.att[5]   = "3"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/C9075RA.e, -p=(1201.0000,2.0000,90.0000), -g=grp_wall, -col=g_colour_stud75, -fil=grey50,\
            -z=2436, -ya=(0.0000,0.0000,-1.0000), -za=(0.0000,1.0000,0.0000)
   this.att[1]   = "STUD"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "C9075ra|0.75|SQUARE"
   this.att[5]   = "4"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/C9075RA.e, -p=(1535.0000,2.0000,90.0000), -g=grp_wall, -col=g_colour_stud75, -fil=grey50,\
            -z=2436, -ya=(0.0000,0.0000,-1.0000), -za=(0.0000,1.0000,0.0000)
   this.att[1]   = "STUD"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "C9075ra|0.75|SQUARE"
   this.att[5]   = "5"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/U904475G550.e, -p=(1.0000,2440.0000,45.0000), -g=grp_wall, -col=g_colour_plate75, -fil=grey50,\
            -z=1908, -ya=(0.0000,0.0000,-1.0000), -za=(1.0000,0.0000,0.0000)
   this.att[1]   = "TOPPLATE"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "U904475G550|0.75|SQUARE"
   this.att[5]   = "6"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 1
   this.fragment = 1



# See line 0 of 
	this = null
   get $HFCMODELS/U904475G550.e, -p=(1909.0000,0.0000,45.0000), -g=grp_wall, -col=g_colour_plate75, -fil=grey50,\
            -z=1908, -ya=(0.0000,0.0000,-1.0000), -za=(-1.0000,0.0000,0.0000)
   this.att[1]   = "BOTPLATE"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "U904475G550|0.75|SQUARE"
   this.att[5]   = "7"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 1
   this.fragment = 1



# See line 0 of 
	this = null
   get $HFCMODELS/U904475G550.e, -p=(6.0000,1122.5000,45.0000), -g=grp_wall, -col=green, -fil=grey50,\
            -z=1898, -ya=(0.0000,0.0000,-1.0000), -za=(1.0000,0.0000,0.0000)
   this.att[1]   = "NOGGING"
   this.att[2]   = ""
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "U904475G550|0.75|SQUARE"
   this.att[5]   = "8"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/BRA-1.2X32-250.e, -p=(-9.7266,-18.4446,92.0000), -g=grp_wall, -col=brown, -fil=grey50,\
            -z=3139.708539, -ya=(-0.7889,0.6145,0.0000), -za=(0.6145,0.7889,0.0000)
   this.att[1]   = "WALLBRACE"
   this.att[2]   = "(-9.727,-18.445,92.000)"
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "BRA-1.2x32-250|1.2|SQUARE"
   this.att[5]   = "9"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/TENSIONER_SET.e, -p=(448.0554,569.2221,94.0000), -g=grp_wall, -col=black, -fil=grey50,\
            -z=80, -ya=(-0.7889,0.6145,0.0000), -za=(0.0000,0.0000,1.0000)
   this.att[1]   = "TENSIONER_SET"
   this.att[2]   = "(-9.727,-18.445,92.000)"
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "TENSIONER_SET"
   this.att[5]   = "10"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/BRA-1.2X32-250.e, -p=(1919.7266,-18.4446,92.0000), -g=grp_wall, -col=brown, -fil=grey50,\
            -z=3139.708539, -ya=(-0.7889,-0.6145,0.0000), -za=(-0.6145,0.7889,0.0000)
   this.att[1]   = "WALLBRACE"
   this.att[2]   = "(1919.727,-18.445,92.000)"
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "BRA-1.2x32-250|1.2|SQUARE"
   this.att[5]   = "11"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# See line 0 of 
	this = null
   get $HFCMODELS/TENSIONER_SET.e, -p=(1461.9446,569.2221,94.0000), -g=grp_wall, -col=black, -fil=grey50,\
            -z=80, -ya=(-0.7889,-0.6145,0.0000), -za=(0.0000,0.0000,1.0000)
   this.att[1]   = "TENSIONER_SET"
   this.att[2]   = "(1919.727,-18.445,92.000)"
   this.att[3]   = "~adata.wallpanel_2"
   this.att[4]   = "TENSIONER_SET"
   this.att[5]   = "12"
   this.att[6]   = "0"
   this.att[7]   = ""
   this.att[8]   = "*Not-created*"
   this.att[9]   = "RollformerID=AllInOne"
   this.rank     = 20
   this.fragment = 0



# Truss to wall tie-downs:


# Wall profile prism. See line 0 of 
	point p[]
	p[1] = (0.000,0.000,0.000)
	p[2] = (0.000,2440.000,0.000)
	p[3] = (1910.000,2440.000,0.000)
	p[4] = (1910.000,0.000,0.000)
	this = null
	prism u:90, -g=grp_wall, -col=g_colour_open_dovetail_lb, -ls = g_ls_open_dovetail, { p }
	this.att[1] = TYPE_WALLPROFILE
	this.att[2] = "~adata.wallpanel_2"
	this.att[3] = "~adata.floor_1"
	this.att[5] = "2"

# See line 0 of 
	this = null
	get $HFCMODELS/arrow, -p=(955.0000,0.0000,90.0000), -g=grp_wall, -s=1.000000
	this.att[1] = "ARROW"
	this.att[3] = "~adata.wallpanel_2"
	this.att[4] = "WallBuilder 1.9.0.0"
	tags = null
	tags[1] = (0.0000,0.0000,0.0000)
	tags[2] = (1910.0000,0.0000,0.0000)
	tags[3] = (1910.0000,0.0000,90.0000)
	tags[4] = (0.0000,0.0000,90.0000)
	tags[5] = (0.0000,0.0000,0.0000)
	tags[6] = (0.0000,2440.0000,0.0000)
	tags[7] = (1910.0000,2440.0000,0.0000)
	tags[8] = (1910.0000,0.0000,0.0000)
	tags[9] = (0.0000,2440.0000,0.0000)
	tags[10] = (1910.0000,2440.0000,0.0000)
	tags[11] = (1910.0000,0.0000,0.0000)
	tags[12] = (0.0000,0.0000,0.0000)

	RUN wall_NCdata ("~adata.wallpanel_2")
	~adata.wallpanel_2.parts_created = 1
	save $HFCJOB\models\wallpanel_2.e, grp_wall, -tag
	kill grp_wall

	this = null
    get $HFCJOB\models\wallpanel_2.e, -p=(2760.0000,850.0000,0.0000), -xa=(0.0000,1.0000,0.0000), -ya=(0.0000,0.0000,1.0000)
    this.att[1] = TYPE_WALLPANEL
    this.att[2] = "~adata.wallpanel_2"
    this.att[3] = "~adata.floor_1"
    this.att[5] = "2"
